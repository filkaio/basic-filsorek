This is basic version of Filsorek with only DHT11 (temperature and humidity
sensor, DHT22 can be used as well), powered by powerbank.

It is designed to be as simple as possible and easy makable using simple
techniques.

# Components

* ESP32-C3-32S DevKit,
* DHT11/DHT22,
* Powerbank


# Manufacturing

Just use copper plates, it will be added in next updates


# Licence

CC-BY-SA-NC
